class Header {
  #element;
  #opener;
  #switcher;
  #hider;
  #activeClass = 'header--active';
  #hiddenClass = 'header--top-hidden';

  constructor(element) {
    this.#element = element;
    this.#opener = this.#element.querySelector('.js-header-opener');
    this.#switcher = this.#element.querySelector('.js-header-switcher');
    this.#hider = this.#element.querySelector('.js-header-hider');
  }

  openMenu = () => {
    this.#element.classList.add(this.#activeClass);
  };

  closeMenu = () => {
    this.#element.classList.remove(this.#activeClass);
  };

  hideTopHeader = () => {
    this.#element.classList.add(this.#hiddenClass);
  };

  init = () => {
    this.#opener.addEventListener('click', this.openMenu);
    this.#switcher.addEventListener('click', this.closeMenu);
    this.#hider.addEventListener('click', this.hideTopHeader);
  };
}

export const header = new Header(document.querySelector('.header'));
