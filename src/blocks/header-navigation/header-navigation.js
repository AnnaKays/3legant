const headerNavigation = () => {
  const parents = document.querySelectorAll('.header-navigation__item--parent');
  Array.from(parents).forEach((parent) => {
    const link = parent.firstElementChild;
    const opener = document.createElement('div');
    opener.classList.add('header-navigation__opener');
    opener.addEventListener('click', (evt) => evt.target.closest('.header-navigation__item').classList.toggle('header-navigation__item--active'));
    link.after(opener);
  });
};

export default headerNavigation;
