class HeaderSearch {
  #element;
  #activeClass = 'header-search--active';

  constructor(element) {
    this.#element = element;
  }

  close = () => this.#element.classList.remove(this.#activeClass);

  #clickHandler = (evt) => {
    if (evt.target.classList.contains('header-search__opener')) this.#element.classList.toggle(this.#activeClass);
  };

  #keydownHandler = (evt) => {
    if (evt.keyCode === 27) this.close();
  };

  init = () => {
    if (this.#element) {
      this.#element.addEventListener('click', this.#clickHandler);
      document.addEventListener('keydown', this.#keydownHandler);
    }
  };
}

export const headerSearch = new HeaderSearch(document.querySelector('.header-search'));
