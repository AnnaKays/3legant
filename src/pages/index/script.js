import { header } from '../../blocks/header/header';
import headerNavigation from '../../blocks/header-navigation/header-navigation';
import { headerSearch } from '../../blocks/header-search/header-search';

window.addEventListener('DOMContentLoaded', () => {
  header.init();
  headerNavigation();
  headerSearch.init();
});
